package com.timber.timbeter;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.fenchtose.tooltip.Tooltip;
import com.fenchtose.tooltip.TooltipAnimation;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


public class Diameter extends CommonFragment {
    RelativeLayout advancedSection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View x = inflater.inflate(R.layout.fragment_diameter, container, false);

        mainActivity = (Activity) getActivity();
        mainContext = getActivity().getBaseContext();

        //Reference size
        initializeSeeker((CrystalSeekbar) x.findViewById(R.id.rangeSeekbar1), (TextView) x.findViewById(R.id.reference_value));

        //Tree species section
        initializeSpinner(getResources().getStringArray(R.array.list_tree_species),
                (MaterialBetterSpinner) x.findViewById(R.id.android_material_design_spinner));

        //Log Length Section
        initializeSeeker((CrystalSeekbar) x.findViewById(R.id.rangeSeekbar2), (TextView) x.findViewById(R.id.textMin1Log));

        //Calculate Using section
        initializeSpinner(getResources().getStringArray(R.array.list_calculate_using),
                (MaterialBetterSpinner) x.findViewById(R.id.calculate));


        //Storage section
        initializeSpinner(getResources().getStringArray(R.array.list_storage),
                (MaterialBetterSpinner) x.findViewById(R.id.storageSpinner));

        //Type section
        initializeSpinner(getResources().getStringArray(R.array.list_type),
                (MaterialBetterSpinner) x.findViewById(R.id.typeSpinner));

        //Quality section
        initializeSpinner(getResources().getStringArray(R.array.list_assortment),
                (MaterialBetterSpinner) x.findViewById(R.id.qualitySpinner));


        CheckBox chkIos = (CheckBox) x.findViewById(R.id.advanced);
        advancedSection = (RelativeLayout) x.findViewById(R.id.storage);
        chkIos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    advancedSection.setVisibility(View.VISIBLE);
                }else{
                    advancedSection.setVisibility(View.GONE);
                }

            }
        });


    /*    showTooltip(v, R.string.bottom_auto_adjust, Tooltip.BOTTOM, true,
                TooltipAnimation.SCALE,
                tooltipSize,
                ViewGroup.LayoutParams.WRAP_CONTENT);*/
        final ViewGroup root = (ViewGroup) x.findViewById(R.id.root);

        x.findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(v,R.string.reference_size_tip, Tooltip.BOTTOM, true,
                        TooltipAnimation.SCALE,
                        mainActivity.getResources().getDimensionPixelOffset(R.dimen.tooltip_width),
                        ViewGroup.LayoutParams.WRAP_CONTENT,root);
            }
        });

        x.findViewById(R.id.imageView1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(v,R.string.log_tip, Tooltip.BOTTOM, true,
                        TooltipAnimation.SCALE,
                        mainActivity.getResources().getDimensionPixelOffset(R.dimen.tooltip_width),
                        ViewGroup.LayoutParams.WRAP_CONTENT,root);
            }
        });

        return x;
    }

}
