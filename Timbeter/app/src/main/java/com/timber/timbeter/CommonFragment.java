package com.timber.timbeter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.fenchtose.tooltip.Tooltip;
import com.fenchtose.tooltip.TooltipAnimation;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class CommonFragment extends Fragment {
    Context mainContext;
    Activity mainActivity;

    protected void initializeSpinner(String[] listValues, MaterialBetterSpinner spinner){

        ArrayAdapter  arrayAdapter = new ArrayAdapter<String>(mainActivity,
                android.R.layout.simple_dropdown_item_1line, listValues);
        spinner.setAdapter(arrayAdapter);

    }

    protected void initializeSeeker(CrystalSeekbar seekbar,final TextView valueText){
        seekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                valueText.setText(String.valueOf(minValue));
            }
        });
    }

    public void showTooltip(@NonNull View anchor, @StringRes int resId,
                             @Tooltip.Position int position, boolean autoAdjust,
                             @TooltipAnimation.Type int type,
                             int width, int height,ViewGroup root) {
        TextView textView = (TextView) mainActivity.getLayoutInflater().inflate(R.layout.tooltip_textview, null);
        textView.setText(resId);
        textView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        showTooltip(anchor, textView, position, autoAdjust, type, Color.parseColor("#009100"),root);
    }

    public void showTooltip(@NonNull View anchor, @NonNull View content,
                             @Tooltip.Position int position, boolean autoAdjust,
                             @TooltipAnimation.Type int type,
                             int tipColor,ViewGroup root) {

        new Tooltip.Builder(mainActivity)
                .anchor(anchor, position)
                .animate(new TooltipAnimation(type, 500))
                .autoAdjust(autoAdjust)
                .content(content)
                .withTip(new Tooltip.Tip(mainActivity.getResources().getDimensionPixelSize(R.dimen.tip),
                        mainActivity.getResources().getDimensionPixelSize(R.dimen.tip), tipColor))
                .into(root)
                .debug(true)
                .show();
    }

}
